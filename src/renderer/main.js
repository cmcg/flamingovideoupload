import Vue from "vue";

import App from "./App";
import router from "./router";
import store from "./store";

import UIkit from "uikit/dist/css/uikit-core.min.css";
import UIkitJS from "uikit/dist/js/uikit.min.js";
import UIkitIcons from "uikit/dist/js/uikit-icons.min.js";

// import Vuikit from 'vuikit'
// import VuikitIcons from '@vuikit/icons'
// import '@vuikit/theme'

// Vue.use(Vuikit)
// Vue.use(VuikitIcons)

import Buefy from "buefy";
import "buefy/dist/buefy.css";
Vue.use(Buefy);

import "@mdi/font/css/materialdesignicons.css";

import Amplify, * as AmplifyModules from "aws-amplify";
import {
  AmplifyPlugin
} from "aws-amplify-vue";
import awsmobile from './aws-exports'
Amplify.configure(awsmobile)

Vue.use(AmplifyPlugin, AmplifyModules);

if (!process.env.IS_WEB) Vue.use(require("vue-electron"));
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  components: {
    App
  },
  router,
  store,
  template: "<App/>"
}).$mount("#app");